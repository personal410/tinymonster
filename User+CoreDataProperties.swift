//
//  User+CoreDataProperties.swift
//  TinyMonsters
//
//  Created by Victor Salazar on 22/01/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import CoreData
extension User{
    @NSManaged var totalGold:NSNumber
}