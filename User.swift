//
//  User.swift
//  TinyMonsters
//
//  Created by Victor Salazar on 22/01/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import Foundation
import CoreData
class User:NSManagedObject{
    class func getUser() -> User {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "User")
        let result = try! ctxt.executeFetchRequest(fetchReq)
        if result.count == 0 {
            let newUser = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: ctxt) as! User
            newUser.totalGold = 500
            try! ctxt.save()
            return newUser
        }else{
            return result.first! as! User
        }
    }
}