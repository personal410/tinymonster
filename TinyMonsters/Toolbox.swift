//
//  Toolbox.swift
//  TinyMonsters
//
//  Created by Victor Salazar on 20/01/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
func +(lhs:CGPoint, rhs:CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
}
func -(lhs:CGPoint, rhs:CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
}
class Toolbox{
    class func showAlertWithMessage(message:String, inViewCont viewCont:UIViewController){
        let alerCont = UIAlertController(title: "Alerta", message: message, preferredStyle: .Alert)
        viewCont.presentViewController(alerCont, animated: true, completion: nil)
    }
}