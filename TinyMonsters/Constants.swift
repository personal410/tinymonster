//
//  Constants.swift
//  TinyMonsters
//
//  Created by Victor Salazar on 21/01/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
let Minute = 60
let Hour = 60 * Minute
let NumberSquareFieldView = 11
let SpaceWidth:CGFloat = 36
let SpaceHeight:CGFloat = SpaceWidth * 0.75
let SingleSpacePath:CGPath = {
    let bezierPath = UIBezierPath()
    bezierPath.moveToPoint(CGPoint(x: SpaceWidth / 2.0, y: 1))
    bezierPath.addLineToPoint(CGPoint(x: SpaceWidth - 1, y: SpaceHeight / 2.0))
    bezierPath.addLineToPoint(CGPoint(x: SpaceWidth / 2.0, y: SpaceHeight - 1))
    bezierPath.addLineToPoint(CGPoint(x: 1, y: SpaceHeight / 2.0))
    bezierPath.addLineToPoint(CGPoint(x: SpaceWidth / 2.0, y: 1))
    return bezierPath.CGPath
}()
let ExpandAnimation:CAKeyframeAnimation = {
    let expandAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
    expandAnimation.values = [1.0, 1.2, 1.0]
    expandAnimation.keyTimes = [0.0, 0.5, 1.0]
    expandAnimation.duration = 0.2
    expandAnimation.removedOnCompletion = true
    return expandAnimation
}()