//
//  Building+CoreDataProperties.swift
//  TinyMonsters
//
//  Created by Victor Salazar on 21/01/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import CoreData
extension Building{
    @NSManaged var buildingId:NSNumber
    @NSManaged var buildingType:NSNumber
    @NSManaged var coordinate:String
    @NSManaged var startProductionDate:NSDate
    @NSManaged var productionRate:NSNumber
    @NSManaged var level:NSNumber
    @NSManaged var startUpgradeDate:NSDate?
    @NSManaged var buildTime:NSNumber
    @NSManaged var upgradeCost:NSNumber
    @NSManaged var capacity:NSNumber
}