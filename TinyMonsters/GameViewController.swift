//
//  ViewController.swift
//  TinyMonsters
//
//  Created by Victor Salazar on 6/01/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
class GameViewController:UIViewController, TMFielViewDelegate{
    //MARK: - Variables
    let user = User.getUser()
    //MARK: - IBOutlet
    @IBOutlet weak var fieldView:TMFieldView!
    @IBOutlet weak var goldAmountLbl:UILabel!
    @IBOutlet weak var buyButton:UIButton!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.fieldView.delegate = self
        self.goldAmountLbl.text = "\(user.totalGold.integerValue)"
        for building in Building.getBuildings() {
            fieldView.addItemViewWithBuilding(building)
        }
        checkCanBuyMoreBuildings()
    }
    //MARK: - IBAction
    @IBAction func addBuilding(){
        if user.totalGold.integerValue >= 150 {
            checkCanBuyMoreBuildings()
            user.totalGold = user.totalGold.integerValue - 150
            self.goldAmountLbl.text = "\(user.totalGold.integerValue)"
            AppDelegate.saveContext()
            TMItemType.GoldMine.hashValue
            let newBuilding = Building.addBuilding(
                TMItemType.GoldMine.hashValue, coordinate: TMCoordinate(nX: 0, nY: 0), productionRate: 200, buildTime: Minute, upgradeCost: 300, capacity: 500)
            fieldView.addItemViewWithBuilding(newBuilding!)
        }else{
            Toolbox.showAlertWithMessage("No hay dinero suficiente", inViewCont: self)
        }
    }
    //MARK: - Auxiliar
    func fieldViewDidRecieveCoins(){
        self.goldAmountLbl.text = "\(user.totalGold.integerValue)"
    }
    func checkCanBuyMoreBuildings(){
        if TMItemType.GoldMine.getMaxCountBuilding() == Building.getBuildings().count {
            self.buyButton.enabled = false
        }
    }
}