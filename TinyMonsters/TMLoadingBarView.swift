//
//  LoadingBarView.swift
//  TinyMonsters
//
//  Created by Victor Salazar on 25/01/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
class TMLoadingBarView:UIView{
    let barProgressView:UIView
    var currentPercentage:CGFloat {
        didSet{
            var frameTemp = self.barProgressView.frame
            frameTemp.size.width = (self.frame.width - 2) * currentPercentage
            self.barProgressView.frame = frameTemp
        }
    }
    override init(frame:CGRect){
        self.barProgressView = UIView(frame: CGRect(x: 1, y: 1, width: 0, height: frame.height - 2))
        self.barProgressView.backgroundColor = UIColor.greenColor()
        self.currentPercentage = 0
        super.init(frame: frame)
        self.addSubview(barProgressView)
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.blackColor().CGColor
        self.layer.cornerRadius = 2
    }
    required init?(coder aDecoder:NSCoder){
        fatalError("init(coder:) has not been implemented")
    }
}