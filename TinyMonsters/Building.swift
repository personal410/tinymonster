//
//  Building.swift
//  TinyMonsters
//
//  Created by Victor Salazar on 20/01/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import Foundation
import CoreData
class Building:NSManagedObject{
    var isBuilding:Bool = {
        return false
    }()
    class func getBuildings() -> Array<Building> {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Building")
        fetchReq.sortDescriptors = [NSSortDescriptor(key: "buildingId", ascending: true)]
        do{
            let results = try ctxt.executeFetchRequest(fetchReq) as! [Building]
            return results
        }catch _ as NSError {
            return []
        }
    }
    class func addBuilding(buildingType:Int, coordinate nCoordinate:TMCoordinate, productionRate nProductionRate:Int, buildTime nBuildTime:Int, upgradeCost nUpgradeCost:Int, capacity nCapacity:Int) -> Building? {
        let ctxt = AppDelegate.getMoc()
        let newBuilding = NSEntityDescription.insertNewObjectForEntityForName("Building", inManagedObjectContext: ctxt) as! Building
        let arrBuildings = self.getBuildings()
        var buildingId = 1
        for buildingTemp in arrBuildings {
            let buildingIdTemp = buildingTemp.buildingId.integerValue
            if buildingIdTemp == buildingId {
                buildingId++
            }else if buildingIdTemp > buildingId {
                break
            }
        }
        newBuilding.buildingId = buildingId
        newBuilding.buildingType = buildingType
        newBuilding.coordinate = nCoordinate.stringValue
        newBuilding.startUpgradeDate = NSDate()
        newBuilding.startProductionDate = NSCalendar.currentCalendar().dateByAddingUnit(.Second, value: nBuildTime, toDate: NSDate(), options: [])!
        newBuilding.productionRate = nProductionRate
        newBuilding.level = 1
        newBuilding.buildTime = nBuildTime
        newBuilding.upgradeCost = nUpgradeCost
        newBuilding.capacity = nCapacity
        try! ctxt.save()
        return newBuilding
    }
    class func updateBuilding(building:Building, newCoordinate:TMCoordinate) -> Bool {
        let ctxt = AppDelegate.getMoc()
        building.coordinate = newCoordinate.stringValue
        do{
            try ctxt.save()
            return true
        }catch{
            return false
        }
    }
}