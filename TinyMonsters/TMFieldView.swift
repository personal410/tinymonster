//
//  FieldView.swift
//  TinyMonsters
//
//  Created by Victor Salazar on 6/01/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
protocol TMFielViewDelegate:class {
    func fieldViewDidRecieveCoins()
}
class TMFieldView:UIView, TMItemViewDelegate{
    var arrSingleSpaceViews:Array<TMSingleSpaceView> = []
    var arrItemViews:Array<TMItemView> = []
    var selectedItemView:TMItemView?
    weak var delegate:TMFielViewDelegate?
    override func didMoveToSuperview(){
        super.didMoveToSuperview()
        let spaceBottom:CGFloat = CGFloat(NumberSquareFieldView - 1) / 2.0
        let startCenter = CGPoint(x: frame.width / 2.0, y: frame.height / 2.0 + spaceBottom * SpaceHeight)
        for index in 0 ..< Int(pow(Float(NumberSquareFieldView), 2)) {
            let x = index % NumberSquareFieldView
            let y = index / NumberSquareFieldView
            let addX = CGFloat(x - y) * SpaceWidth / 2.0
            let addY = CGFloat(x + y) * SpaceHeight / 2.0
            let singleSpaceView = TMSingleSpaceView(nCoordinate: TMCoordinate(nX: x, nY: y))
            singleSpaceView.center = CGPoint(x: startCenter.x + addX, y: startCenter.y - addY)
            self.addSubview(singleSpaceView)
            arrSingleSpaceViews.append(singleSpaceView)
        }
    }
    override func drawRect(rect:CGRect){
        super.drawRect(rect)
        let bezierPath = UIBezierPath()
        let semiWidth = SpaceWidth * CGFloat(NumberSquareFieldView) / 2.0
        let semiHeight = SpaceHeight * CGFloat(NumberSquareFieldView) / 2.0
        bezierPath.moveToPoint(self.center - CGPoint(x: 0, y: semiHeight))
        bezierPath.addLineToPoint(self.center + CGPoint(x: semiWidth, y: 0))
        bezierPath.addLineToPoint(self.center + CGPoint(x: 0, y: semiHeight))
        bezierPath.addLineToPoint(self.center - CGPoint(x: semiWidth, y: 0))
        bezierPath.addLineToPoint(self.center - CGPoint(x: 0, y: semiHeight))
        let ctxt = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(ctxt, 1)
        CGContextSetFillColorWithColor(ctxt, UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1).CGColor)
        CGContextSetStrokeColorWithColor(ctxt, UIColor.blackColor().CGColor)
        CGContextAddPath(ctxt, bezierPath.CGPath)
        CGContextDrawPath(ctxt, .FillStroke)
        CGContextFillPath(ctxt)
        CGContextStrokePath(ctxt)
    }
    func addItemViewWithBuilding(building:Building){
        let itemView = TMItemView(building: building, fieldView: self)
        self.arrItemViews.append(itemView)
        self.addSubview(itemView)
    }
    override func touchesEnded(touches:Set<UITouch>, withEvent event:UIEvent?){
        let touch = touches.first!
        for itemView in arrItemViews {
            for placedSingleSpaceView in itemView.arrPlacedSingleSpaceView {
                let point = touch.locationInView(placedSingleSpaceView)
                if placedSingleSpaceView.pointInsideView(point) {
                    self.selectedItemView?.canMove = false
                    if itemView == selectedItemView {
                        self.selectedItemView = nil
                    }else{
                        self.selectedItemView = itemView
                        self.selectedItemView?.canMove = true
                    }
                    return
                }
            }
        }
        self.selectedItemView?.canMove = false
        self.selectedItemView = nil
    }
    func itemViewDidReceivedCoins() {
        delegate?.fieldViewDidRecieveCoins()
    }
}