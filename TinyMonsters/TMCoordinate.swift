//
//  TMCoordinate.swift
//  TinyMonsters
//
//  Created by Victor Salazar on 14/01/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import Foundation
class TMCoordinate:CustomStringConvertible{
    var x:Int
    var y:Int
    init(nX:Int, nY:Int){
        self.x = nX
        self.y = nY
    }
    var description:String {
        return "{x: \(x), y: \(y)}"
    }
    var stringValue:String {
        return "\(x),\(y)"
    }
    init(string:String){
        let arrCoordinates = string.componentsSeparatedByString(",")
        let nX = (arrCoordinates[0] as NSString).integerValue
        let nY = (arrCoordinates[1] as NSString).integerValue
        self.x = nX
        self.y = nY
    }
}
func ==(left:TMCoordinate, right:TMCoordinate) -> Bool {
    return left.x == right.x && left.y == right.y
}
func >(left:TMCoordinate, right:TMCoordinate) -> Bool {
    return left.x > right.x || left.y > right.y
}
func <(left:TMCoordinate, right:TMCoordinate) -> Bool {
    return left.x < right.x || left.y < right.y
}