//
//  ItemView.swift
//  TinyMonsters
//
//  Created by Victor Salazar on 8/01/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
enum TMItemType:Int{
    case ElixirStorage, BigElixirStorage, GoldMine
    func getImageName() -> String {
        let arrImageNames = ["ElixirStorage", "ElixirStorage", "GoldMine"]
        return arrImageNames[self.hashValue]
    }
    func getSize() -> Int {
        let arrSizes = [1, 2, 3]
        return arrSizes[self.hashValue]
    }
    func getMaxLevel() -> Int {
        let arrMaxLevels = [1, 1, 5]
        return arrMaxLevels[self.hashValue]
    }
    func getMaxCountBuilding() -> Int {
        let arrMaxCountBuilding = [1, 1, 4]
        return arrMaxCountBuilding[self.hashValue]
    }
}
protocol TMItemViewDelegate:class {
    func itemViewDidReceivedCoins()
}
class TMItemView:UIView{
    //MARK: - Variables
    var originalImage:UIImage
    var imageView:UIImageView
    var arrPlacedSingleSpaceView:Array<TMSingleSpaceView> = []
    var itemType:TMItemType
    var building:Building
    weak var fieldView:TMFieldView?
    var arrCurrentSingleSpaceViews:Array<TMSingleSpaceView> = []
    var loadingBarView:TMLoadingBarView?
    var loadingBarTimer:NSTimer?
    var ableGetCointTimer:NSTimer?
    var getCoinsButton:UIButton
    var upgradeButton:UIButton
    weak var delegate:TMItemViewDelegate?
    var canMove = false {
        didSet{
            self.imageView.image = originalImage.tintPhoto(canMove ? UIColor.darkGrayColor() : UIColor.clearColor())
            self.getCoinsButton.hidden = !canMove
            self.upgradeButton.hidden = !canMove
            if canMove {
                let startProducionDate = self.building.startProductionDate
                let productionTime = NSCalendar.currentCalendar().components(.Second, fromDate: startProducionDate, toDate: NSDate(), options: []).second
                let coins = Int(floor(CGFloat(productionTime * self.building.productionRate.integerValue) / 3600.0))
                if coins > 1 {
                    self.getCoinsButton.enabled = true
                }else{
                    let timeToGetOneCoin:Double = 3600.0 / self.building.productionRate.doubleValue - Double(productionTime)
                    self.ableGetCointTimer?.invalidate()
                    self.ableGetCointTimer = NSTimer.scheduledTimerWithTimeInterval(timeToGetOneCoin, target: self, selector: "ableGetCoinBtn", userInfo: nil, repeats: false)
                    self.getCoinsButton.enabled = false
                }
                self.layer.addAnimation(ExpandAnimation, forKey: "expand")
            }else{
                if self.arrCurrentSingleSpaceViews.count > 0 {
                    if self.arrCurrentSingleSpaceViews.first!.type == TMSingleSpaceViewState.NoPermitted {
                        for currentSingleSpaceViewTemp in arrCurrentSingleSpaceViews {
                            currentSingleSpaceViewTemp.type = .Normal
                        }
                        self.alignToSingleSpaceView(self.arrPlacedSingleSpaceView.first!)
                        self.arrCurrentSingleSpaceViews = []
                    }
                }
            }
        }
    }
    //MARK: - Init
    init(building nBuilding:Building, fieldView nFieldView:TMFieldView){
        self.delegate = nFieldView
        self.building = nBuilding
        self.itemType = TMItemType(rawValue: nBuilding.buildingType.integerValue)!
        self.fieldView = nFieldView
        let image = UIImage(named: self.itemType.getImageName() + "\(self.building.level.integerValue)")!
        self.originalImage = image
        let size = self.itemType.getSize()
        let newWidth = SpaceWidth * CGFloat(size)
        let newHeight = newWidth * image.size.height / image.size.width
        self.imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        self.imageView.image = originalImage.tintPhoto(UIColor.clearColor())
        self.getCoinsButton = UIButton(frame: CGRect(x: 100, y: self.fieldView!.frame.height - 30, width: SpaceWidth * 2 , height: 20))
        self.upgradeButton = UIButton(frame: CGRect(x: 110 + SpaceWidth * 2, y: self.fieldView!.frame.height - 30, width: SpaceWidth * 4 , height: 20))
        super.init(frame: self.imageView.frame)
        self.getCoinsButton.setTitle("Recoger", forState: .Normal)
        self.getCoinsButton.backgroundColor = UIColor.blackColor()
        self.getCoinsButton.hidden = true
        self.getCoinsButton.addTarget(self, action: "getCoins", forControlEvents: .TouchUpInside)
        self.getCoinsButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        self.getCoinsButton.setTitleColor(UIColor.lightGrayColor(), forState: .Disabled)
        self.upgradeButton.setTitle("Actualizar (\(self.building.upgradeCost.integerValue))", forState: .Normal)
        self.upgradeButton.backgroundColor = UIColor.blackColor()
        self.upgradeButton.hidden = true
        self.upgradeButton.addTarget(self, action: "upgradeBuilding", forControlEvents: .TouchUpInside)
        self.upgradeButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        self.upgradeButton.setTitleColor(UIColor.lightGrayColor(), forState: .Disabled)
        self.fieldView!.addSubview(self.getCoinsButton)
        self.fieldView!.addSubview(self.upgradeButton)
        self.clipsToBounds = false
        self.addSubview(imageView)
        self.userInteractionEnabled = true
        let panGesture = UIPanGestureRecognizer(target: self, action: "panItem:")
        self.addGestureRecognizer(panGesture)
        let totalCount = Int(pow(CGFloat(size), 2))
        let initialCoordinate = TMCoordinate(string: self.building.coordinate)
        for index in 0 ..< totalCount {
            let movX = index % size
            let movY = index / size
            let newCoordiante = TMCoordinate(nX: (initialCoordinate.x + movX), nY: (initialCoordinate.y + movY))
            let arrSingleSpaceViewsFilter = fieldView!.arrSingleSpaceViews.filter(){$0.coordinate == newCoordiante}
            let singleSpaceViewTemp = arrSingleSpaceViewsFilter.first!
            singleSpaceViewTemp.itemView = self
            self.arrPlacedSingleSpaceView.append(singleSpaceViewTemp)
        }
        self.alignToSingleSpaceView(arrPlacedSingleSpaceView.first!)
        self.checkIsBuilding()
        self.upgradeButton.enabled = !(self.itemType.getMaxLevel() == self.building.level.integerValue)
    }
    required init?(coder aDecoder:NSCoder){
        fatalError("init(coder:) has not been implemented")
    }
    //MARK: - Actions
    func upgradeLoadingBar(){
        let startDate = self.building.startUpgradeDate!
        let seconds = CGFloat(NSCalendar.currentCalendar().components(.Second, fromDate: startDate, toDate: NSDate(), options: []).second)
        let builTime = CGFloat(self.building.buildTime.doubleValue)
        if seconds <= builTime {
            loadingBarView!.currentPercentage = seconds / builTime
        }else{
            self.upgradeButton.enabled = !(self.itemType.getMaxLevel() == self.building.level.integerValue)
            self.building.isBuilding = false
            loadingBarTimer?.invalidate()
            self.loadingBarView?.removeFromSuperview()
            self.loadingBarView = nil
        }
    }
    func getCoins(){
        let startProducionDate = self.building.startProductionDate
        let productionTime = NSCalendar.currentCalendar().components(.Second, fromDate: startProducionDate, toDate: NSDate(), options: []).second
        var coins = Int(floor(CGFloat(productionTime * self.building.productionRate.integerValue)) / 3600.0)
        self.ableGetCointTimer?.invalidate()
        if coins >= self.building.capacity.integerValue {
            coins = self.building.capacity.integerValue
            self.building.startProductionDate = NSDate()
            let timeToGetOneCoin:Double = 3600.0 / self.building.productionRate.doubleValue
            NSTimer.scheduledTimerWithTimeInterval(timeToGetOneCoin, target: self, selector: "ableGetCoinBtn", userInfo: nil, repeats: false)
        }else{
            let time:Int =  Int(floor(3600.0 * Double(coins) / self.building.productionRate.doubleValue))
            self.building.startProductionDate = NSCalendar.currentCalendar().dateByAddingUnit(.Second, value: time, toDate: self.building.startProductionDate, options: [])!
            let timeToGetOneCoin:Double = Double(productionTime - time)
            NSTimer.scheduledTimerWithTimeInterval(timeToGetOneCoin, target: self, selector: "ableGetCoinBtn", userInfo: nil, repeats: false)
        }
        let user = User.getUser()
        user.totalGold = user.totalGold.integerValue + coins
        AppDelegate.saveContext()
        delegate?.itemViewDidReceivedCoins()
        self.getCoinsButton.enabled = false
    }
    func upgradeBuilding(){
        let user = User.getUser()
        if user.totalGold.integerValue >= self.building.upgradeCost.integerValue {
            user.totalGold = user.totalGold.integerValue - self.building.upgradeCost.integerValue
            if self.building.buildingType.integerValue == 2 {
                if self.building.level.integerValue == 1 {
                    self.building.upgradeCost = 300
                    self.building.buildTime = 5 * Minute
                    self.building.capacity = 1000
                    self.building.productionRate = 400
                    self.building.level = 2
                }else if self.building.level.integerValue == 2 {
                    self.building.upgradeCost = 700
                    self.building.buildTime = 15 * Minute
                    self.building.capacity = 1500
                    self.building.productionRate = 600
                    self.building.level = 3
                }else if self.building.level.integerValue == 3 {
                    self.building.upgradeCost = 1400
                    self.building.buildTime = Hour
                    self.building.capacity = 2500
                    self.building.productionRate = 800
                    self.building.level = 4
                }else if self.building.level.integerValue == 4 {
                    self.building.upgradeCost = -1
                    self.building.buildTime = 0
                    self.building.capacity = 10000
                    self.building.productionRate = 1000
                    self.building.level = 5
                }
                self.building.startUpgradeDate = NSDate()
                self.building.startProductionDate = NSCalendar.currentCalendar().dateByAddingUnit(.Second, value: self.building.buildTime.integerValue, toDate: self.building.startUpgradeDate!, options: [])!
            }
            let image = UIImage(named: self.itemType.getImageName() + "\(self.building.level.integerValue)")!
            self.originalImage = image
            self.imageView.image = originalImage.tintPhoto(UIColor.darkGrayColor())
            AppDelegate.saveContext()
            self.checkIsBuilding()
        }
    }
    //MARK: - Pan Gesture
    func panItem(panGesture:UIPanGestureRecognizer){
        if canMove {
            if panGesture.state == .Began || panGesture.state == .Changed {
                if self.arrCurrentSingleSpaceViews.count == 0 {
                    self.arrCurrentSingleSpaceViews = self.arrPlacedSingleSpaceView
                }
                let currentCoor = self.arrCurrentSingleSpaceViews.first!.coordinate
                let arrSingleSpaceViews = fieldView!.arrSingleSpaceViews
                let numberLoops = Int(floor(CGFloat(NumberSquareFieldView + 1) / 2.0))
                for currentLoop in 1 ... numberLoops {
                    let size = currentLoop * 2 - 1
                    let totalCount = Int(pow(CGFloat(size), 2))
                    for index in 0 ..< totalCount {
                        let x = index % size
                        let y = index / size
                        if (x > 0 && x < size - 1) && (y > 0 && y < size - 1) {
                            continue
                        }
                        let diffX = x - currentLoop + 1
                        let diffY = y - currentLoop + 1
                        let newCoor = TMCoordinate(nX: currentCoor.x + diffX, nY: currentCoor.y + diffY)
                        if (newCoor.x >= 0 && newCoor.x <= (NumberSquareFieldView - self.itemType.getSize())) && (newCoor.y >= 0 && newCoor.y <= (NumberSquareFieldView - self.itemType.getSize())) {
                            let arrSingleSpaceViewFilter = arrSingleSpaceViews.filter(){$0.coordinate == newCoor}
                            if arrSingleSpaceViewFilter.count > 0 {
                                let singleSpaceViewTemp = arrSingleSpaceViewFilter.first!
                                var pointTemp = panGesture.locationInView(singleSpaceViewTemp)
                                pointTemp.y += CGFloat(size - 1) * SpaceHeight / 2.0
                                if pointTemp.x < 0 || pointTemp.x > SpaceWidth || pointTemp.y < 0 || pointTemp.y > SpaceHeight {
                                    continue
                                }
                                if singleSpaceViewTemp.pointInsideView(pointTemp) {
                                    if singleSpaceViewTemp == self.arrCurrentSingleSpaceViews.first! {
                                        if self.arrCurrentSingleSpaceViews.first!.type == .Normal {
                                            for currentSingleSpaceViewTemp in arrCurrentSingleSpaceViews {
                                                currentSingleSpaceViewTemp.type = .Permitted
                                                fieldView!.bringSubviewToFront(currentSingleSpaceViewTemp)
                                            }
                                        }
                                        fieldView!.bringSubviewToFront(self)
                                    }else{
                                        let size = self.itemType.getSize()
                                        let totalCount = Int(pow(CGFloat(size), 2.0))
                                        for currentSingleSpaceViewTemp in arrCurrentSingleSpaceViews {
                                            currentSingleSpaceViewTemp.type = .Normal
                                        }
                                        arrCurrentSingleSpaceViews = [singleSpaceViewTemp]
                                        let initialCoordinate = singleSpaceViewTemp.coordinate
                                        var isPermitted = true
                                        for index2 in 0 ..< totalCount {
                                            let movX = index2 % size
                                            let movY = index2 / size
                                            let newCoordiante = TMCoordinate(nX: (initialCoordinate.x + movX), nY: (initialCoordinate.y + movY))
                                            let arrSingleSpaceViewsFilter = fieldView!.arrSingleSpaceViews.filter(){$0.coordinate == newCoordiante}
                                            let singleSpaceViewTemp2 = arrSingleSpaceViewsFilter.first!
                                            arrCurrentSingleSpaceViews.append(singleSpaceViewTemp2)
                                            fieldView!.bringSubviewToFront(singleSpaceViewTemp2)
                                            if isPermitted {
                                                if singleSpaceViewTemp2.itemView != nil {
                                                    if singleSpaceViewTemp2.itemView != self {
                                                        isPermitted = false
                                                    }
                                                }
                                            }
                                        }
                                        self.fieldView!.bringSubviewToFront(self)
                                        self.alignToSingleSpaceView(arrCurrentSingleSpaceViews.first!)
                                        for currentSingleSpaceViewTemp in arrCurrentSingleSpaceViews {
                                            currentSingleSpaceViewTemp.type = isPermitted ? .Permitted : .NoPermitted
                                        }
                                    }
                                    return
                                }
                            }
                        }
                    }
                }
            }else if panGesture.state == .Ended {
                if arrCurrentSingleSpaceViews.first!.type == .Permitted {
                    for currentSingleSpaceViewTemp in arrCurrentSingleSpaceViews {
                        currentSingleSpaceViewTemp.type = .Normal
                    }
                    if arrCurrentSingleSpaceViews.first != arrPlacedSingleSpaceView.first {
                        for placedSingleSpaceViewTemp in arrPlacedSingleSpaceView {
                            placedSingleSpaceViewTemp.itemView = nil
                        }
                        self.arrPlacedSingleSpaceView = self.arrCurrentSingleSpaceViews
                        Building.updateBuilding(self.building, newCoordinate: self.arrPlacedSingleSpaceView.first!.coordinate)
                        for placedSingleSpaceViewTemp in arrPlacedSingleSpaceView {
                            placedSingleSpaceViewTemp.itemView = self
                        }
                        self.arrCurrentSingleSpaceViews = []
                    }
                }
                let arrItemViews = self.fieldView!.arrItemViews
                for itemView in arrItemViews {
                    fieldView?.bringSubviewToFront(itemView)
                }
                for index in 0 ..< arrItemViews.count - 1 {
                    let firstItemView = arrItemViews[index]
                    let secondItemView = arrItemViews[index + 1]
                    let firstCoordinate = firstItemView.arrPlacedSingleSpaceView.first!.coordinate
                    let secondCoordinate = secondItemView.arrPlacedSingleSpaceView.first!.coordinate
                    if firstCoordinate < secondCoordinate {
                        fieldView?.bringSubviewToFront(firstItemView)
                        
                    }
                }
                for itemView in arrItemViews {
                    if itemView.building.isBuilding {
                        self.fieldView?.bringSubviewToFront(itemView.loadingBarView!)
                    }
                }
            }
        }
    }
    //MARK: - Auxiliar
    func checkIsBuilding(){
        let startDate = self.building.startUpgradeDate!
        let seconds = CGFloat(NSCalendar.currentCalendar().components(.Second, fromDate: startDate, toDate: NSDate(), options: []).second)
        let builTime = CGFloat(self.building.buildTime.doubleValue)
        if seconds <= builTime {
            self.upgradeButton.enabled = false
            self.building.isBuilding = true
            loadingBarView = TMLoadingBarView(frame: CGRect(x: frame.width / 2.0 - 30, y: frame.height + 5, width: 62, height: 15))
            loadingBarView!.currentPercentage = seconds / builTime
            self.addSubview(loadingBarView!)
            loadingBarTimer = NSTimer.scheduledTimerWithTimeInterval(Double(self.building.buildTime.integerValue / 60), target: self, selector: "upgradeLoadingBar", userInfo: nil, repeats: true)
        }else{
            self.building.isBuilding = false
        }
    }
    func ableGetCoinBtn(){
        self.getCoinsButton.enabled = true
    }
    func alignToSingleSpaceView(sinSpaView:TMSingleSpaceView){
        var centerSingleSpaceView = sinSpaView.center
        //centerSingleSpaceView.y -= CGFloat(self.itemType.getSize() - 1) / 2.0 * SpaceHeight
        centerSingleSpaceView.y -= (self.frame.size.height - sinSpaView.frame.height) / 2.0
        self.center = centerSingleSpaceView
    }
}