//
//  SingleSpaceView.swift
//  TinyMonsters
//
//  Created by Victor Salazar on 6/01/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
enum TMSingleSpaceViewState{
    case Normal, Permitted, NoPermitted
    func getTypeColor() -> UIColor {
        let arrColors = [UIColor.clearColor(), UIColor.greenColor(), UIColor.redColor()]
        return arrColors[self.hashValue]
    }
}
class TMSingleSpaceView:UIView{
    var coordinate:TMCoordinate
    weak var itemView:TMItemView?
    var type:TMSingleSpaceViewState = .Normal {
        didSet{
            self.setNeedsDisplay()
        }
    }
    init(nCoordinate:TMCoordinate){
        self.coordinate = nCoordinate
        super.init(frame: CGRectMake(0, 0, SpaceWidth, SpaceHeight))
        self.backgroundColor = UIColor.clearColor()
    }
    required init?(coder aDecoder:NSCoder){
        fatalError("init(coder:) has not been implemented")
    }
    override func drawRect(rect:CGRect){
        super.drawRect(rect)
        let ctxt = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(ctxt, 2)
        CGContextSetFillColorWithColor(ctxt, self.type.getTypeColor().CGColor)
        CGContextSetStrokeColorWithColor(ctxt, UIColor.clearColor().CGColor)
        CGContextAddPath(ctxt, SingleSpacePath)
        CGContextDrawPath(ctxt, .FillStroke)
        CGContextFillPath(ctxt)
        CGContextStrokePath(ctxt)
    }
    func pointInsideView(point:CGPoint) -> Bool {
        return CGPathContainsPoint(SingleSpacePath, nil, point, true)
    }
}